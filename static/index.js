$(document).ready(function()
{
    $('form[name="search"]').on("submit", function(e) 
    {
        window.console.log("KEK");
        e.preventDefault();
    });

    function addImageClick()
    {
        $("#list > form").each(function(e) {
            $(this.img).click(function() 
            {
                $.ajax(
                {
                    url: '/recommend',
                    data: $(this.parentElement).serialize(),
                    type: 'POST',
                    success: function(response) 
                    {
                        $('#list').html(response);
                        addImageClick();
                    },
                    error: function(error) 
                    {
                        console.log(error);
                    }
                });    
            }); 
        });
    }

    $('.tab').each(function(e) {
        $(this).click(function() {
            updateFavourites();
        });
    });

    $("#generate-button-2").click(function() {
        $.ajax(
        {
            url: '/favourite_recommend',
            type: 'POST',
            success: function(response) 
            {
                $('#list-2').html(response);
            },
            error: function(error) 
            {
                console.log(error);
            }
        });    
    });

    $("#generate-button-3").click(function() {
        $.ajax(
        {
            url: '/comparative_recommend',
            type: 'POST',
            success: function(response) 
            {
                $('#list-3').html(response);
            },
            error: function(error) 
            {
                console.log(error);
            }
        });    
    });

    $("#list2 > form").each(function(e) {
        $(this.img).click(function() 
        {
            $.ajax(
            {
                url: '/recommend',
                data: $(this.parentElement).serialize(),
                type: 'POST',
                success: function(response) 
                {
                    $('#list').html(response);
                    addImageClick();
                },
                error: function(error) 
                {
                    console.log(error);
                }
            });    
        }); 
    });

    $("#list2-2 > form").each(function(e) {
        $(this.querySelector("#heart")).click(function() 
        {
            $(this).toggleClass("favorite no-favorite");

            $.ajax(
            {
                url: '/favorite',
                data: $(this.parentElement).serialize(),
                type: 'POST',
                success: function(response) 
                {
                    
                },
                error: function(error) 
                {
                    console.log(error);
                }
            });    

            updateFavourites();
        }); 
    });

    $('input#search').keyup(function() {
        $("#list2 > form").each(function(e) {
            var string = $('input#search')[0].value;
            var regex = new RegExp(string, "ig");

            if (regex.test(this.title.value))
            {
                this.style.display = "block"
            }
            else
            {
                this.style.display = "none"
            }
        });
    });

    $('input#search-2').keyup(function() {
        $("#list2-2 > form").each(function(e) {
            var string = $('input#search-2')[0].value;
            var regex = new RegExp(string, "ig");

            if (regex.test(this.title.value))
            {
                this.style.display = "block"
            }
            else
            {
                this.style.display = "none"
            }
        });
    });

    $('input#search-3').keyup(function() {
        $("#list2-3 > form").each(function(e) {
            var string = $('input#search-3')[0].value;
            var regex = new RegExp(string, "ig");

            if (regex.test(this.title.value))
            {
                this.style.display = "block"
            }
            else
            {
                this.style.display = "none"
            }
        });
    });

    $('#item-col > li').each(function(e) {
        $(this).click(function(e) {
            $('#item-col > li').each(function(e) {
                if ($(this).hasClass("item-selected"))
                {
                    $(this).toggleClass("item-selected");
                }
            });

            $(this).toggleClass("item-selected");


            var name = this.innerText;

            $.ajax(
            {
                url: '/change_user',
                data: name,
                contentType: "text/plain",
                type: 'POST',
                success: function(response) 
                {
                    console.log("Cheerio");
                },
                error: function(error) 
                {
                    console.log(error);
                }
            });

            updateFavourites();
        });
    });

    $("#list2-3 > form").each(function(e) {
        $(this.querySelector("#heart")).click(function() 
        {
            $(this).toggleClass("favorite no-favorite");

            $.ajax(
            {
                url: '/favorite_user',
                data: $(this.parentElement).serialize(),
                type: 'POST',
                success: function(response) 
                {
                    console.log(response);
                },
                error: function(error) 
                {
                    console.log(error);
                }
            });    

            updateFavourites();
        }); 
    });

    addImageClick();

    function updateFavourites()
    {
        $.ajax(
        {
            url: '/check_favourites',
            success: function(response)
            {
                var obj = $.parseJSON(response);

                $.each(obj, function(k, v) {
                    var inputField = $('#list2-3 > form').find("input[value="+k+"]")[0];
                    var heartElement = $(inputField).siblings()[3];

                    if (v == 0)
                    {
                        $(heartElement).removeClass('favorite').addClass('no-favorite');
                    }
                    else
                    {
                        $(heartElement).removeClass('no-favorite').addClass('favorite');
                    }
                });

                $.each(obj, function(k, v) {
                    var inputField = $('#list2-2 > form').find("input[value="+k+"]")[0];
                    var heartElement = $(inputField).siblings()[3];

                    if (v == 0)
                    {
                        $(heartElement).removeClass('favorite').addClass('no-favorite');
                    }
                    else
                    {
                        $(heartElement).removeClass('no-favorite').addClass('favorite');
                    }
                });
            }
        });
    }
});