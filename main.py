from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
import logging
import json
import random
import time
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)
games = []
global current_user

class Favorites(db.Model):
    __tablename__ = 'favorites'
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), primary_key=True)
    game_id = db.Column(db.Integer, db.ForeignKey('games.id'), primary_key=True)
    favorite = db.Column(db.Integer, default=0)

    def __init__(self, user_id=None, game_id=None, favorite=0):
        self.user_id = user_id
        self.game_id = game_id
        self.favorite = favorite

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    favo = db.relationship('Favorites', lazy='dynamic', backref='user', primaryjoin=id == Favorites.user_id)

    def add_to_favorites(self, game_id, favorite):
        self.favo.append(Favorites(self.id, game_id, favorite))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Name %r>' % self.name

class Game(db.Model):
    __tablename__ = 'games'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=True)
    genres = db.Column(db.String(120))
    image = db.Column(db.String(80), unique=True)
    favo = db.relationship('Favorites', lazy='dynamic', backref='game', primaryjoin=id == Favorites.game_id) 

    def __init__(self, title, genres, image):
        self.title = title
        self.genres = genres
        self.image = image

    def __repr__(self):
        return '<Title %r>' % self.title

@app.route("/recommend", methods=['POST'])
def recommend():
    #Get id of the game
    num = int(request.form['id'])
    num = num - 1

    #Get all games and find the picked game
    games = Game.query.all()
    choice = games[num]

    choice_genres_array = choice.genres.split(",")
    game_desire = 0

    multiplier = 0.5
    multiplier_choice = 1.0
    for game in games:
        multiplier = 0.5
        genres_array = game.genres.split(",")
        
        # Go over every genre and compare them to other games genres assigning desire depending on similarity
        index = 0
        game_desire = 0
        for genre in genres_array:
            while index < len(choice_genres_array):
                if genre == choice_genres_array[index]:
                    game_desire += (multiplier + multiplier_choice) / 2
                index += 1
                multiplier_choice = multiplier_choice * 0.8
            index = 0
            multiplier_choice = 1.0
            multiplier = multiplier * 0.6
        game.desire = game_desire

    # Sort games based on their desire, highest to lowest
    games = sorted(games, key=lambda x: x.desire, reverse=True)

    return rendered_games_list(games[:11], True)

@app.route("/favorite", methods=['POST'])
def favorite():
    #Get the game
    num = int(request.form['id'])
    num = num - 1

    #Get all the games, pick the selected one
    games = Game.query.all()
    choice = games[num]

    #Get the user and his list of favourites
    user = User.query.all()[0]
    cur_fav = user.favo.filter_by(user_id=user.id).filter_by(game_id=choice.id).first().favorite

    #If the game is already favourited, unfavourite it, otherwise favourite it
    if cur_fav == 0:
        user.favo.filter_by(user_id=user.id).filter_by(game_id=choice.id).first().favorite = 1
    else:
        user.favo.filter_by(user_id=user.id).filter_by(game_id=choice.id).first().favorite = 0

    db.session.commit()

    return "success"

def get_genre_profile(user, games):
    #Get list of all favourite games
    favourited_games = user.favo.filter_by(user_id=user.id).filter_by(favorite=1).all()
    list_fav_games = []

    for game in favourited_games:
        list_fav_games.append(games[game.game_id - 1])

    genre_profile = dict()

    #Go over every game in the favourite list, and give every genre that is in there a value saying how much the user likes it, the more times it repeats, the higher the value
    for game in list_fav_games:
        genres_array = game.genres.split(",")

        genre_multiplier = 1
        for genre in genres_array:
            if genre in genre_profile:
                genre_profile[genre] += genre_multiplier
            else:
                genre_profile[genre] = genre_multiplier
            genre_multiplier = genre_multiplier * 0.8

    list_obj = [genre_profile, list_fav_games]

    return list_obj

@app.route("/favourite_recommend", methods=['POST'])
def get_favourite():

    #Get all the games and the user
    games = Game.query.all()
    user = User.query.all()[0]

    #Create User Profile for the user based on his favourites
    list_obj = get_genre_profile(user, games)
    genre_profile = list_obj[0]
    list_fav_games = list_obj[1]
    
    #Go over every game in the database and give it desire depending on how it compares to the User Profile
    game_desire = 0
    multiplier = 0.5
    for game in games:
        if game in list_fav_games:
            game.desire = -1
        else:
            multiplier = 0.5
            game_desire = 0

            game_genre_array = game.genres.split(",")
            for genre in game_genre_array:
                if genre in genre_profile:
                    game_desire += genre_profile[genre] * multiplier

                multiplier = multiplier * 0.6
            
            game.desire = game_desire
            
    #Sort the game by desire
    games = sorted(games, key=lambda x: x.desire, reverse=True)

    return rendered_games_list(games[:10], False)

def rendered_games_list(games, contains_pick):     
    return render_template('list.html',
                           games=games,
                           con_pick=contains_pick)

@app.route("/change_user", methods=['POST'])
def change_user():

    #Change the current user in the system
    user_name = request.data
    global current_user 
    user = User.query.filter_by(name=user_name).first()
    
    current_user = user

    return "success"

@app.route("/favorite_user", methods=['POST'])
def favorite_user():
    num = int(request.form['id'])
    num = num - 1

    games = Game.query.all()
    choice = games[num]

    user = User.query.all()[current_user.id - 1]
    cur_fav = user.favo.filter_by(user_id=user.id).filter_by(game_id=choice.id).first().favorite

    if cur_fav == 0:
        user.favo.filter_by(user_id=user.id).filter_by(game_id=choice.id).first().favorite = 1
    else:
        user.favo.filter_by(user_id=user.id).filter_by(game_id=choice.id).first().favorite = 0

    db.session.commit()

    return "success"

@app.route("/check_favourites", methods=['GET'])
def check_favourite():
    # Get all games and the current user
    games = Game.query.all()
    users = User.query.all()[current_user.id - 1]

    # Create a dictionary to hold values that tell us if a game is favourited by the user, a dictionary is easily converted into json unlike the query result from the database
    json_data = dict()

    # loop over every game and add it to the dictionary with an int declaring if it's favourited or not
    for game in games: 
        x = users.favo.filter_by(user_id=current_user.id).filter_by(game_id=game.id).first().favorite
        json_data[game.id] = x

    # return the now jsonified dictionary as a response to the call
    return json.dumps(json_data, ensure_ascii=False)

@app.route("/comparative_recommend", methods=['POST'])
def comp_rec():
    games = Game.query.all()
    users = User.query.all()
    global current_user

    main_user = users[current_user.id - 1]

    genre_profiles = dict()

    #genre_profile[0] = genre_profile, genre_profile[1] = list of favourites

    users = sorted(users, key=lambda x: (x.name!=main_user.name, x.name))

    #Give every user in the system a User Profile
    user_desire = 0
    for user in users:
        genre_profiles[user.name] = get_genre_profile(user, games)

        for genre, weight in genre_profiles[user.name][0].iteritems():
            if genre in genre_profiles[main_user.name][0]:
                user_desire += abs(weight - genre_profiles[main_user.name][0][genre])
            else:
                user_desire += weight
        
        user.desire = user_desire
        user_desire = 0
        
    #Sort users in order of desire, so the more similiar users are first
    users = sorted(users, key=lambda x: x.desire, reverse=False)

    recommended_games = []

    #Put favourites from other users in a list, with the more similar users first, the order of each subsection of the list is randomized
    for user in users:
        if user != main_user:
            random.shuffle(genre_profiles[user.name][1])
            for game in genre_profiles[user.name][1]:
                if game not in genre_profiles[main_user.name][1]:
                    recommended_games.append(game)

    return rendered_games_list(recommended_games[:10], False)

@app.route("/")
def main():
    db.create_all()
    populate()

    user = User.query.all()[0]
    games = Game.query.all()
    games = sorted(games, key=lambda x: x.title, reverse=False)

    global current_user 
    
    current_user = user

    for game in games: 
        x = user.favo.filter_by(user_id=user.id).filter_by(game_id=game.id).first().favorite
        game.favorite = x

    return render_template('index.html',
                           title='Home',
                           gameslist=games)

def populate():
    dat = "Database"

    #This is the raw data entered into the database, in case of the database being deleted, uncomment

    # user1 = User("John")
    # user2 = User("David")
    # user3 = User("Michael")
    # user4 = User("Elsa")
    # user5 = User("Andrew")

    # user_list = [user1, user2, user3, user4, user5]

    # game1 = Game('NieR Automata', 'Great Soundtrack,Action,Story Rich,Hack and Slash,Female Protagonist,RPG,Post-apocalyptic,Open World,Anime,Robots,JRPG,Singleplayer,Atmospheric,Sci-fi,Bullet Hell,Adventure,Character Action Game,Spectacle fighter,Violent,Nudity', 'niera')
    # game2 = Game('Dark Souls III', 'Dark Fantasy,Difficult,Atmospheric,RPG,Lore-Rich,Third Person,Exploration,Story Rich,PvP,Co-op,Action RPG,Adventure,Open World,Action,Great Soundtrack,Multiplayer,Singleplayer,Character Customization,Horror,Replay Value', 'ds3')
    # game3 = Game('Grand Theft Auto V', 'Open World,Action,Multiplayer,First-Person,Third Person,Crime,Adventure,Shooter,Third-Person Shooter,Singleplayer,Racing,Atmospheric,Mature,Sandbox,Co-op,Great Soundtrack,Funny,Comedy,Moddable,RPG', 'gtav')
    # game4 = Game('Dark Souls', 'RPG,Dark Fantasy,Difficult,Action,Exploration,Fantasy,Lore-Rich,Adventure,Atmospheric,Third Person,Action RPG,Open World,Replay Value,Multiplayer,Dark,Great Soundtrack,Story Rich,Singleplayer,Character Customization,Medieval', 'ds')
    # game5 = Game('Dark Souls II', 'RPG,Dark Fantasy,Difficult,Action RPG,Action,Adventure,Exploration,Atmospheric,Co-op,Multiplayer,Fantasy,PvP,Third Person,Replay Value,Open World,Medieval,Dark,Online Co-Op,Singleplayer,Horror', 'ds2')
    # game6 = Game('Hyper Light Drifter', 'Pixel Graphics,Great Soundtrack,Difficult,Adventure,Atmospheric,Action,Indie,Singleplayer,RPG,Hack and Slash,Exploration,Action RPG,Colorful,2D,Fantasy,Post-apocalyptic,Top-Down,Sci-fi,Kickstarter,Metroidvania', 'hld')
    # game7 = Game('The Witcher 3: Wild Hunt','Open World,RPG,Story Rich,Atmospheric,Mature,Fantasy,Adventure,Choices Matter,Singleplayer,Third Person,Great Soundtrack,Action,Nudity,Medieval,Dark Fantasy,Multiple Endings,Magic,Action RPG,Dark,Sandbox','w3')
    # game8 = Game('The Elder Scrolls V: Skyrim', 'Open World,RPG,Fantasy,Adventure,Moddable,Dragons,First-Person,Action,Medieval,Magic,Singleplayer,Story Rich,Great Soundtrack,Sandbox,Lore-Rich,Atmospheric,Character Customization,Third Person,Action RPG,Dark Fantasy', 'tessv')
    # game9 = Game("Tom Clancy's Ghost Recon Wildlands", 'Open World,Shooter,Action,Co-op,Tactical,Multiplayer,Stealth,Third-Person Shooter,Military,Adventure,Singleplayer,Third Person,Online Co-Op,FPS,Sandbox,Strategy,First-Person,Atmospheric,Survival,PvP', "tcgrw")
    # game10 = Game("Dragon's Dogma: Dark Arisen", 'RPG,Open World,Character Customization,Action,Adventure,Fantasy,Singleplayer,Dark Fantasy,Third Person,Dragons,Action RPG,Magic,Great Soundtrack,Exploration,Atmospheric,Difficult,Story Rich,JRPG,Multiplayer,Walking Simulator', 'ddda')
    # game11 = Game("The Witcher 2: Assassins of Kings", 'RPG,Fantasy,Mature,Choices Matter,Story Rich,Third Person,Singleplayer,Dark Fantasy,Adventure,Nudity,Multiple Endings,Action,Atmospheric,Based On A Novel,Open World,Difficult,Dark,Medieval,Magic,Nonlinear', 'w2')
    # game12 = Game("Bayonetta", "Action,Female Protagonist,Hack and Slash,Singleplayer,Great Soundtrack,Character Action Game,Spectacle fighter,Adventure,Nudity,Sexual Content,Beat 'em up,Classic,Fantasy,Mature,Anime,Fast-Paced,Gore,Atmospheric,Comedy", 'ba')
    # game13 = Game("Planescape: Torment", "RPG,Story Rich,Adventure,Dark Fantasy,Strategy,Fantasy,Party-Based RPG,Atmospheric,Classic,Tactical RPG,Isometric,Great Soundtrack,CRPG,Singleplayer", "pst")
    # game14 = Game("Rocket League", "Multiplayer,Racing,Soccer,Sports,Competitive,Team-Based,Football,Online Co-Op,Action,Co-op,Fast-Paced,Funny,Great Soundtrack,Local Multiplayer,Local Co-Op,Split Screen,Singleplayer,4 Player Local,Casual,Indie", "rl")
    # game15 = Game("Yooka-Laylee", "3D Platformer,Adventure,Kickstarter,Indie,Colorful,Platformer,Great Soundtrack,Family Friendly,Action,Exploration,Cute,Funny,Singleplayer,Retro,Casual,Open World,Comedy,Multiplayer,Female Protagonist,Horror", "yl")
    # game16 = Game("Civilization V", "Turn-Based Strategy,Strategy,Turn-Based,Multiplayer,Historical,4X,Singleplayer,Hex Grid,Grand Strategy,Replay Value,Co-op,Tactical,Economy,Moddable,Diplomacy,Simulation,Great Soundtrack,Classic,Touch-Friendly,Education", "civ5")
    # game17 = Game("Civilization VI", "Strategy,Turn-Based Strategy,Historical,Multiplayer,Singleplayer,Turn-Based,4X,Grand Strategy,Cartoony,War,Simulation,Tactical,Great Soundtrack,Moddable,Online Co-Op,Open World,Cartoon,Co-op,Casual,Memes", "civ6")
    # game18 = Game("Planet Coaster", "Simulation,Building,Management,Sandbox,Singleplayer,Strategy,Family Friendly,Great Soundtrack,Funny,Realistic,Atmospheric,Casual,Open World,Colorful,Base Building,Multiplayer,Adventure,Economy,Action,First-Person", "pc")
    # game19 = Game("Dota 2", "Free to Play,MOBA,Strategy,Multiplayer,Team-Based,Action,e-sports,Online Co-Op,Competitive,PvP,RTS,Difficult,RPG,Fantasy,Tower Defense,Co-op,Character Customization,Replay Value,Action RPG,Simulation", "d2")
    # game20 = Game("Street Fighter V", "Fighting,Competitive,Multiplayer,2D Fighter,Action,Arcade,Local Multiplayer,e-sports,2.5D,Anime,Great Soundtrack,Singleplayer,Classic,2D,Female Protagonist,Beat 'em up", "sf5")
    # game21 = Game("Street Fighter IV", "Fighting,Action,Arcade,Multiplayer,Local Multiplayer,Beat 'em up,2D Fighter,2.5D,Competitive", "sf4")
    # game22 = Game("Injustice: Gods Among Us", "Fighting,Superhero,Action,Multiplayer,Batman,Comic Book,Singleplayer,Local Multiplayer,Controller,Beat 'em up,2D Fighter,Arcade,2.5D,Competitive,Story Rich,2D,Atmospheric,Gore,3D Vision,Great Soundtrack", "inj")
    # game23 = Game("Ultimate Marvel Vs. Capcom 3", "Fighting,Action,2D Fighter,Multiplayer,Arcade,Superhero,Comic Book,Competitive,Anime,Great Soundtrack", "umvc3")
    # game24 = Game("Devil May Cry 4", "Action,Hack and Slash,Character Action Game,Great Soundtrack,Adventure,Stylized,Singleplayer,Spectacle fighter,Third Person,Beat 'em up,Difficult,Controller,Fantasy,Demons,Atmospheric,RPG", "dmc4")
    # game25 = Game("Devil May Cry 3", "Action,Hack and Slash,Stylized,Great Soundtrack,Spectacle fighter,Character Action Game,Classic,Difficult,Singleplayer,Third Person,Beat 'em up,Adventure,Fantasy,Story Rich,Atmospheric", "dmc3")
    # game26 = Game("Counter Strike Global Offensive", "FPS,Multiplayer,Shooter,Action,Team-Based,Competitive,Tactical,First-Person,e-sports,PvP,Online Co-Op,Military,Co-op,Strategy,War,Trading,Realistic,Difficult,Fast-Paced,Moddable", "csgo")
    # game27 = Game("Stellaris", "Space,Strategy,Grand Strategy,Sci-fi,4X,Exploration,Multiplayer,Real-Time with Pause,Singleplayer,RTS,Simulation,Sandbox,Great Soundtrack,Replay Value,Moddable,Atmospheric,Open World,Story Rich,Building,Adventure", "stl")
    # game28 = Game("Prey", "Action,Sci-fi,Space,Horror,FPS,First-Person,Singleplayer,Atmospheric,Aliens,Adventure,Story Rich,Futuristic,Exploration,Open World,Psychological Horror,Stealth,Survival Horror,RPG,Violent,Great Soundtrack", "pr")
    # game29 = Game("DOOM", "FPS,Action,Gore,Demons,Shooter,First-Person,Multiplayer,Great Soundtrack,Singleplayer,Fast-Paced,Sci-fi,Classic,Horror,Atmospheric,Difficult,Remake,Zombies,Co-op,Blood,Memes", "dm")
    # game30 = Game("Warhammer 40,000: Dawn of War III", "Warhammer 40K,Strategy,RTS,Sci-fi,Action,Multiplayer,Games Workshop,Gore,MOBA,Singleplayer,Atmospheric,War,Base Building,Tactical,Co-op,Story Rich,Great Soundtrack,Classic,Violent,Casual", "dow3")
    # game31 = Game("Dragon Quest Heroes II", "RPG,Action,Anime,Hack and Slash,JRPG,Co-op,Open World,Adventure", "dqh2")
    # game32 = Game("Tales of Zestiria", "JRPG,Anime,RPG,Fantasy,Story Rich,Open World,Action RPG,Adventure,Great Soundtrack,Singleplayer,Action,Multiplayer,Co-op,Action-Adventure,Local Co-Op,Cute,Atmospheric,Hack and Slash,Female Protagonist,Third Person", "toz")
    # game33 = Game("Halo Wars", "Strategy,RTS,Sci-fi,Multiplayer,Singleplayer,Action,Great Soundtrack,Co-op,Classic", "hw")
    # game34 = Game("BlazBlue Centralfiction", "Action,Anime,Great Soundtrack,2D Fighter,Fighting,Visual Novel,Story Rich", "bbc")
    # game35 = Game("Half-Life", "FPS,Classic,Action,Sci-fi,Singleplayer,Shooter,Aliens,First-Person,1990's,Multiplayer,Adventure,Story Rich,Atmospheric,Silent Protagonist,Great Soundtrack,Moddable,Linear,Retro,Difficult,Funny", "hl")
    # game36 = Game("Half-Life 2", "FPS,Action,Sci-fi,Classic,Singleplayer,Story Rich,Shooter,Adventure,First-Person,Dystopian,Atmospheric,Zombies,Aliens,Great Soundtrack,Silent Protagonist,Physics,Multiplayer,Moddable,Horror,Puzzle", "hl2")
    # game37 = Game("BioShock", "FPS,Action,Atmospheric,Story Rich,Singleplayer,Steampunk,Horror,First-Person,Shooter,RPG,Sci-fi,Classic,Adventure,Dystopian,Underwater,Political,Dark,Great Soundtrack,Alternate History,Action RPG", "bs")
    # game38 = Game("BioShock 2", "FPS,Action,Singleplayer,Atmospheric,Story Rich,Shooter,First-Person,Horror,Steampunk,Sci-fi,Multiplayer,Underwater,RPG,Adventure,Dystopian,Dark,Great Soundtrack,Action RPG,Philisophical,Linear", "bs2")
    # game39 = Game("BioShock Infinite", "FPS,Action,Story Rich,Singleplayer,Steampunk,Atmospheric,Shooter,First-Person,Alternate History,Adventure,Great Soundtrack,Dystopian,Sci-fi,Time Travel,Fantasy,Linear,Gore,RPG,Political,Controller", "bsi")
    # game40 = Game("The Elder Scrolls IV: Oblivion", "RPG,Open World,Fantasy,Singleplayer,Moddable,First-Person,Exploration,Adventure,Great Soundtrack,Action,Magic,Sandbox,Atmospheric,Character Customization,Story Rich,Medieval,Action RPG,Classic,Third Person,Dark Fantasy", "teso")
    # game41 = Game("The Elder Scrolls III: Morrowind", "RPG,Open World,Fantasy,Classic,Great Soundtrack,Moddable,First-Person,Adventure,Singleplayer,Cult Classic,Atmospheric,Magic,Sandbox,Story Rich,Exploration,Action RPG,Character Customization,Medieval,Action,Third Person", "tesm")
    # game42 = Game("Undertale", "Great Soundtrack,Story Rich,Choices Matter,Funny,RPG,Multiple Endings,Pixel Graphics,Singleplayer,Indie,Comedy,Replay Value,2D,Bullet Hell,Cute,Memes,Retro,Dark,Psychological Horror,Dating Sim,Horror", "unt")
    # game43 = Game("Dishonored", "Stealth,First-Person,Action,Steampunk,Assassin,Singleplayer,Atmospheric,Story Rich,Adventure,Multiple Endings,Dark,Magic,Dystopian,FPS,RPG,Replay Value,Fantasy,Open World,Shooter,Sci-fi", "dis")
    # game44 = Game("Dishonored 2", "Stealth,Action,First-Person,Assassin,Steampunk,Singleplayer,Atmospheric,Story Rich,Female Protagonist,Magic,Parkour,Supernatural,Adventure,Open World,Great Soundtrack,Heist,Gore,Exploration,Dystopian,Illuminati", "dis2")
    # game45 = Game("Fallout 3", "Open World,Post-apocalyptic,RPG,Singleplayer,Exploration,First-Person,Sci-fi,Moddable,Action,Adventure,Atmospheric,FPS,Shooter,Third Person,Character Customization,Sandbox,Story Rich,Action RPG,Horror,Female Protagonist", "fl3")
    # game46 = Game("Fallout: New Vegas", "Open World,RPG,Post-apocalyptic,Singleplayer,Moddable,Action,First-Person,Exploration,Adventure,Sci-fi,FPS,Story Rich,Atmospheric,Western,Sandbox,Action RPG,Survival,Character Customization,Shooter,Third Person", "flnv")
    # game47 = Game("Fallout 4", "Post-apocalyptic,Open World,Exploration,Singleplayer,RPG,Atmospheric,Adventure,Shooter,Story Rich,First-Person,Sci-fi,Action,FPS,Action RPG,Great Soundtrack,Third Person,Sandbox,Third-Person Shooter,Survival,Casual", "fl4")
    # game48 = Game("Portal", "Puzzle,First-Person,Singleplayer,Sci-fi,Comedy,Female Protagonist,Action,Funny,Platformer,FPS,Story Rich,Short,Physics,Classic,Adventure,Science,Atmospheric,Dark Humor,Great Soundtrack,Strategy", "port")
    # game49 = Game("Portal 2", "Puzzle,Co-op,First-Person,Comedy,Sci-fi,Singleplayer,Adventure,Online Co-Op,Funny,Science,Female Protagonist,Action,Story Rich,Multiplayer,Atmospheric,FPS,Local Co-Op,Strategy,Space,Platformer", "port2")
    # game50 = Game("Spelunky", "Rogue-like,Platformer,Indie,Difficult,2D,Local Co-Op,Replay Value,Procedural Generation,Action,Perma Death,Adventure,Rogue-lite,Singleplayer,Controller,Co-op,Local Multiplayer,Great Soundtrack,Side Scroller,Multiplayer,Retro", "spl")

    # game_list = [game1, game2, game3, game4, game5, game6, game7, game8, game9, game10, game11, game12, game13, game14, game15, game16, game17, game18, game19, game20, game21, game22, game23, game24, game25,
    #              game26, game27, game28, game29, game30, game31, game32, game33, game34, game35, game36, game37, game38, game39, game40, game41, game42, game43, game44, game45, game46, game47, game48, game49,
    #              game50]

    # for game in game_list:
    #     db.session.add(game)

    # for user in user_list:
    #     db.session.add(user)

    # db.session.commit()

    # for game in game_list:
    #     for user in user_list:
    #         user.add_to_favorites(game.id, 0)

    # db.session.commit()

if __name__ == "__main__":
    app.run()