This is the final year project I've done for my University degree.

It's a recommender system written in Python with Flask and HTML/CSS/JavaScript.

It recommends video games to users depending on their favourites, as well as favourites of other people.